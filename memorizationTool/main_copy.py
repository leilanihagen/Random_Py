import random # imports random module

class Card: # creates card class
    
   def __init__(self, front, back):
       self.front = front
       self.back = back

   def toString(self):
      text = "Front: " + self.front
      text = text + "Back: " + self.back     
      return text
   

   def quizBack(self):
      print("Front: " + self.front)
      answer = input("Enter your answer for back: ")
      if answer == self.back:
         print("yay")
      else:
         print("nope. Answer is: " + self.back)

   def quizFront(self):
      print("Back: " + self.back)
      answer = input("Enter your answer for front: ")
      if answer == self.front:
         print("perfecto")
      else:
         print("nooooooo... answer is: " + self.front)
   
cards = [] # create a new empty list

## read text file and generate a list of cards

currentFile = input("Specify the name of the card stack file you would like to use: ")

try: # using try in order to catch exceptions
   
   with open(currentFile) as f: # opens "medical.cards" file and allows access via variable "f".
      lines = f.readlines() # reads all lines from file "f" and stores them in the "lines" container as strings.
      
      # begins for loop. Will execute one time per line in "lines."
      # Also creates variable: "currentLine," which holds different objects sequentially for each iteration.
      for currentLine in lines: 
         currentLine = currentLine.rstrip() # removes white space from end of the string.
         if len(currentLine) == 0 or currentLine[0] == "#": #checks if line is empty or comment/beginning with "#".
            pass
         else:
            ###print(currentLine)
            # telling this string: "hey go split yourself" at the comma characters.
            # And store the split strings in the "words" list.
            words = currentLine.split(",")
            ###print(words)
            # instanciates a new Card object and provides 2 strings to it's constructor (__init__).
            newCard = Card(words[0], words[1])
            cards.append(newCard) # append the new card to the "cards" list.
            if (len(currentLine) + int(1)) == len(lines):
               print("\nSuccessfully loaded your card stack!\n")
            
## error with code block below!
# if the file could not be opened, program execution continues here. And "error" contains useful info about the problem.
except Exception as error:
   print("Oooooooops, could not load the file! Restart program and try again.")
   raise SystemExit

## this is the quiz logic

answer = input("Would you like to be quizzed on the answers for the fronts or backs of your cards? type f or b: ")

# determine if we should quiz the Front or Back card.
if answer == "F" or answer == "f":
   quizzingFace = "Front"
elif answer == "B" or answer == "b":
   quizzingFace = "Back"
elif answer != "F" or answer != "f" or answer != "B" or answer != "b":
   quizzingFace = None # assigns some value to quizzingFace so that the program still recongnises it as defined
      
random.shuffle(cards) # shuffle the cards, randomly

for someCard in cards: # iterate over all cards, iteration variable is named "someCard"
   if quizzingFace == "Front": # check if we're quizzing Front?
      someCard.quizFront() # call the quizFront method
   elif quizzingFace == "Back":
      someCard.quizBack()
   else: # must have gotten invalid answer for "f or b"
      pass

def Invalid():
   global answer
   del answer
   answer = input("Invaid. Please enter f or b: ")
   if answer == "F" or answer == "f":
      quizzingFace = "Front"
   elif answer == "B" or answer == "b":
      quizzingFace = "Back"
   elif answer != "F" or answer != "f" or answer != "B" or answer != "b":
      quizzingFace = "null"
   

Invalid()
   




   
