print("\t\t~Medical terminology memorization script!~")
print("\nType in the correct term to match the definition given.\n")

ortho = input("straighten: ")
if ortho == "ortho":
    print("correct\n")
else:
    print("Incorrect\n")

peri = input("surrounding/around: ")
if peri == "peri":
    print("correct\n")
else:
    print("Incorrect\n")
    
cardio = input("heart: ")
if cardio == "cardio":
    print("correct\n\n")
else:
    print("Incorrect\n")
    
pulmo = input("lungs: ")
if pulmo == "pulmo":
    print("correct\n")
else:
    print("Incorrect\n")
   
uro = input("urinary tract/urine: ")
if uro == "uro":
    print("correct\n")
else:
    print("Incorrect\n")
   
chiro = input("hands: ")
if chiro == "chiro":
    print("correct\n")
else:
    print("Incorrect\n")
  
donto = input("tooth: ")
if donto == "donto":
    print("correct\n")
else:
    print("Incorrect\n")
  
pedo = input("child: ")
if pedo == "pedo":
    print("correct\n")
else:
    print("Incorrect\n")
  
podoPedo = input("foot: ")
if podoPedo == "podo/pedo":
    print("correct\n")
else:
    print("Incorrect\n")
  
ophthalmo = input("eye: ")
if ophthalmo == "ophthalmo":
    print("correct\n")
else:
    print("Incorrect\n")
  
neuro = input("nerve: ")
if neuro == "neuro":
    print("correct\n")
else:
    print("Incorrect\n")
  
gastro = input("stomach: ")
if gastro == "gastro":
    print("correct\n")
else:
    print("Incorrect\n")
  
gyneco = input("female reproductive: ")
if gyneco == "gyneco":
    print("correct\n")
else:
    print("Incorrect\n")
  
nephro = input("kidney: ")
if nephro == "nephro":
    print("correct\n")
else:
    print("Incorrect\n")
  
patho = input("disease: ")
if patho == "patho":
    print("correct\n")
else:
    print("Incorrect\n")
  
radio = input("x-ray: ")
if radio == "radio":
    print("correct\n")
else:
    print("Incorrect\n")
  
pyscho = input("mind: ")
if pyscho == "pyscho":
    print("correct\n")
else:
    print("Incorrect\n")
  
onco = input("cancer/tumor: ")
if onco == "onco":
    print("correct\n")
else:
    print("Incorrect\n")
  
crino = input("to secrete: ")
if crino == "crino":
    print("correct\n")
else:
    print("Incorrect\n")
  
entero = input("intestines: ")
if entero == "entero":
    print("correct\n")
else:
    print("Incorrect\n")
  
obstetro = input("pregnancy/childbirth: ")
if obstetro == "obstetro":
    print("correct\n")
else:
    print("Incorrect\n")
  
print("\n\t\tPART TWO - FULL WORDS w/ SUFFIXES\n")

  
cardiology = input("study of the heart: ")
if cardiology == "cardiology":
    print("correct\n")
else:
    print("Incorrect\n")
  
pulmonologist = input("specialist of the lungs: ")
if pulmonologist == "pulmonologist":
    print("correct\n")
else:
    print("Incorrect\n")
  
chiropractor = input("specialist of using hands to manipulate back: ")
if chiropractor == "chiropractor":
    print("correct\n")
else:
    print("Incorrect\n")
  
endodontist = input("specialist of root canals: ")
if endodontist == "endodontist":
    print("correct\n")
else:
    print("Incorrect\n")
  
orthodontist = input("specialist of straigtening teeth: ")
if orthodontist == "orthodontist":
    print("correct\n")
else:
    print("Incorrect\n")
  
ophthalmology = input("study of eyes: ")
if ophthalmology == "ophthalmology":
    print("correct\n")
else:
    print("Incorrect\n")
  
neurologist = input("specialist of nerves: ")
if neurologist == "neurologist":
    print("correct\n")
else:
    print("Incorrect\n")
  
nephrology = input("study of kidneys: ")
if nephrology == "nephrology":
    print("correct\n")
else:
    print("Incorrect\n")
  
gastroentrologist = input("splecialist of stomach and intestines: ")
if gastroenterologist == "gastroenterologist":
    print("correct\n")
else:
    print("Incorrect\n")
  
pathology = input("study of disease: ")
if pathology == "pathology":
    print("correct\n")
else:
    print("Incorrect\n")
  
radiologist = input("specialist of x-rays: ")
if radiologist == "radiologist":
    print("correct\n")
else:
    print("Incorrect\n")
  
orthopedics = input("pertaining to bones and joints: ")
if orthopedics == "orthopedics":
    print("correct\n")
else:
    print("Incorrect\n")
  
cardiologist = input("specialist of the heart: ")
if cardiologist == "cardiologist":
    print("correct\n")
else:
    print("Incorrect\n")
  
urology = input("study of urinary system: ")
if urology == "urology":
    print("correct\n")
else:
    print("Incorrect\n")
  
gynecology = input("study of female reproductive system: ")
if gynecology == "gynecology":
    print("correct\n")
else:
    print("Incorrect\n")
  
periodontist = input("specialist of gums (around teeth): ")
if periodontist == "periodontist":
    print("correct\n")
else:
    print("Incorrect\n")
  
pediatrician = input("child doctor: ")
if pediatritian == "pediatritian":
    print("correct\n")
else:
    print("Incorrect\n")
  
ophthalmologist = input("eye specialist: ")
if ophthalmologist == "ophthalmologist":
    print("correct\n")
else:
    print("Incorrect\n")
  
gastrology = input("study of stomach: ")
if gastrology == "gastrology":
    print("correct\n")
else:
    print("Incorrect\n")
  
nephrologist = input("specialist of kidneys: ")
if nephrologist == "nephrologist":
    print("correct\n")
else:
    print("Incorrect\n")
  
endocrinology = input("stufy of hormone secreting glands: ")
if endocrinology == "endocrinology":
    print("correct\n")
else:
    print("Incorrect\n")
  
pathologist = input("specialist of disease: ")
if pathologist == "pathologist":
    print("correct\n")
else:
    print("Incorrect\n")
  
oncology = input("study of cancer and tumors: ")
if oncology == "oncology":
    print("correct\n")
else:
    print("Incorrect\n")
  
orthopedist = input("specialist of bones and joints: ")
if orthopedist == "orthopedist":
    print("correct\n")
else:
    print("Incorrect\n")
  
pulmonology = input("study of lungs: ")
if pulmonologist == "pulmonologist":
    print("correct\n")
else:
    print("Incorrect\n")
  
urologist = input("specialist of urinary tract/system: ")
if urologist == "urologist":
    print("correct\n")
else:
    print("Incorrect\n")
  
gynecologist = input("specialist of female reproductive system: ")
if gynecologist == "gynecologist":
    print("correct\n")
else:
    print("Incorrect\n")
  
pedodontist = input("child dentist: ")
if pedodontist == "pedodontist":
    print("correct\n")
else:
    print("Incorrect\n")
  
podiatrist = input("specialist of feet: ")
if podiatrist == "podiatrist":
    print("correct\n")
else:
    print("Incorrect\n")
  
neurology = input("study of nerves: ")
if neurology == "neurology":
    print("correct\n")
else:
    print("Incorrect\n")
  
gastrologist = input("specialist of stomach: ")
if gastrologist == "gastrologist":
    print("correct\n")
else:
    print("Incorrect\n")
  
gastroenterology = input("study of stomach and intestines: ")
if gastroenterology == "gastroenterology":
    print("correct\n")
else:
    print("Incorrect\n")
  
endocrinologist = input("specialist of secreting glands: ")
if endocrinologist == "endocrinelogist":
    print("correct\n")
else:
    print("Incorrect\n")
  
radiology = input("study of x-ray: ")
if radiology == "radiology":
    print("correct\n")
else:
    print("Incorrect\n")
  
oncologist = input("specialist of cancer/tumors: ")
if oncologist == "oncologist":
    print("correct\n")
else:
    print("Incorrect\n")

print("\nThank you for using this tool\n")
