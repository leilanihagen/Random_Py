#!/usr/bin/python

import random

SPACER = ", or "

class Card: # creates card class
    
   def __init__(self, front, back):
       self.front = front
       self.back = back

   def toString(self):
      return "Front: " + str(self.front) + "Back: " + str(self.back)        

   def quizBack(self):
      global SPACER
      print("Front: " + SPACER.join(self.front))
      answer = input("Enter your answer for back: ")
      correct = False
      for i in self.back:
         if i == answer:
            correct = True
      if correct:
         print("Correct!")
      else:
            print("Incorrect. Answer is:\n\n" + SPACER.join(self.back) + "\n")

   def quizFront(self):
      global SPACER
      print("Back: " + SPACER.join(self.back))
      answer = input("Enter your answer for front: ")
      correct = False
      for i in self.front:
         if i == answer:
            correct = True
      if correct:
         print("Correct!")
      else:
         print("Incorrect. Answer is:\n\n" + SPACER.join(self.front) + "\n")

## determine if we should quiz the Front or Back of card. Loop breaks when valid input is given.
quizzingFace = None
def SelectMode():
   global quizzingFace
   invalid = True
   while invalid:
      answer = input("Would you like to be quizzed on the answers for the fronts or backs of your cards? type f or b: ")
      if answer == "F" or answer == "f":
         invalid = False
         quizzingFace = "Front"
      elif answer == "B" or answer == "b":
         invalid = False
         quizzingFace = "Back"
   
cards = [] # create a new empty list

currentFile = input("Specify the name of the card stack file you would like to use: ")

## read text file and generate a list of cards

try: # using try in order to catch exceptions
   
   with open(currentFile) as f: # opens "medical.cards" file and allows access via variable "f".
      
      # begins for loop. Will execute one time per line in "lines."
      # Also creates variable: "currentLine," which holds different objects sequentially for each iteration.
      for line in f:
         line = line.rstrip() # removes white space from end of the string.
         if len(line) == 0 or line[0] == "#": #checks if line is empty or comment/beginning with "#".
            pass
         else:
            # telling this string: "hey go split yourself" at the semicolon characters.
            # And store the split strings in the "words" list.
            sides = line.split(";")
            frontWords = sides[0].split(",")
            backWords = sides[1].split(",")
            newCard = Card(frontWords, backWords)
            cards.append(newCard) # append the new card to the "cards" list.
   print("\nSuccessfully loaded your card stack!\n")

# if the file could not be opened, program execution continues here. And "error" contains useful info about the problem.
except Exception as error:
   print("Oooooooops, could not load the file! Restart program and try again.")
   raise SystemExit

## this is the quiz logic

SelectMode()
      
random.shuffle(cards) # shuffle the cards, randomly

for someCard in cards: # iterate over all cards, iteration variable is named "someCard"
   if quizzingFace == "Front":
      someCard.quizFront()
   elif quizzingFace == "Back":
      someCard.quizBack()

