first_name = "stephanie"
last_name = "dearing"

full_name = first_name + " " + last_name

print("Hello, ", full_name, "!") # Notice difference made when commas are used...
print("Hello," + full_name + "!") # Versus when + is used...
print("\n\nHello, " + full_name.title() + "!\n")