# Leilani Hagen 12/5/2016
## Testing the variable naming rules to find out where it is legal to use numbers.

# This is allowed
int3 = 3

# This is not allowed
4int = 4