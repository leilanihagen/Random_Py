guests = ['Barack Obama', 'Michael Jackson', 'Ellen Degeneres']

print('\nI am hosting a dinner party! ' + guests[0].title() + ', ' + guests[1].title() + ', ' + guests[2].title() + ' are all invited!')

no_rsvp = guests.pop(1)
print('\nIt looks like ' + no_rsvp + " can't make it." )

guests.insert(1, 'Ruby Rose')
print('\n ' + guests[1].title() + ' will be attending instead! Get ready for some heat...')

print('\nWith a bigger venue, we can now invite 3 more guests to my honor role party!!!')
guests.insert(0, 'Nicholas Cage')
guests.insert(2, 'Katy Perry')
guests.insert(5, 'Elon Musk')

print('\nOur new guest list is as follows: ' + str(guests))

print('\nUPDATE: We only have space for 2 guests.')

popped_guest1 = guests.pop()
print('Sorry, ' + str(popped_guest1) + ", we can't have you.")

popped_guest2 = guests.pop()
print('Sorry, ' + str(popped_guest2) + ", we can't have you.")

popped_guest3 = guests.pop()
print('Sorry, ' + str(popped_guest3) + ", we can't have you.")

popped_guest4 = guests.pop()
print('Sorry, ' + str(popped_guest4) + ", we can't have you.")

print('\nOur guest list is now down to the two final guests, ' + str(guests[0]) + ' and ' + str(guests[1]) + '.\n')

del guests[0]
del guests[0]

print(str(guests))

print('\n\n\n\n\n' + str(len(guests)))

temp = [1,2,2,33,1,1]
print(str(len(temp)))