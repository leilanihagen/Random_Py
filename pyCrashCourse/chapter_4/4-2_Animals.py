seacreatures = ['Seacuccumber', 'Whale', 'Starfish', 'Nemo']

for creature in seacreatures:
	print(creature)

print('All of these creatures live in the SEA.' +
' Not in lakes, not in puddles, not even in your bathtub, kids!\n')

for creature in seacreatures:
	if creature == 'Starfish':
		print('All ' + creature.lower() + ' live in the great big, salty sea!\n')
	else:
		print('All ' + creature.lower() + 's live in the great big, salty sea!\n')