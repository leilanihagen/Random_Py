import math

#a = int(input("Enter 'a':"))
#b = int(input("Enter 'b':"))
#c = int(input("Enter 'c':"))

#x1 = (-b+math.sqrt(b**2 - 4*a*c)) / (2*a)
#x2 = (-b-math.sqrt(b**2 - 4*a*c)) / (2*a)

#print(x1, x2)
#exit


# Lets the user specify the parameters for the calculations we will be making later.
# ac and b refer to a trinomial in the format ax^2 + bx + c, and this is important
# because the intended use of this program is to find the two integers that you can
# split up that single b term into (so that you have two b terms) to make a 4-term
# polynomial that can be factored using the traditional grouping method.
ac = int(input("Enter 'ac', the integer that you would like x and y to multiply to:"))
b = int(input("Enter 'b', the integer that you would like x and y to sum to:"))

# x and y are the integers that meet these specifications: x*y = ac, and x+y = b.
# These are what the user wants. They are empty integer variables for now.
x,y = int(),int()

# use the quadratic formula to find x
x = (-b+math.sqrt((b**2)-(4*(ac))))/(-2)

y = b-x

# give result
print(x, "and", y, "are the two numbers you can use to factor your trinomial by breaking it into 4 terms and using the grouping method!")