#introduction
user = input("Please enter your name") #learns user's name
print("\nWelcome,", user ,"to Pointless Interrogation(TM)!") #welcomes user to the program

#interrogation
icecream = input("What's your favorite ice cream flavor?")
formality = input("What's your formal title (e.g. miss, Mr., Prof.)?")
animal = input("What's your favorite animal?")
residence = input("Where do you live?")
enemy = input("Who's your enemy?")
snack = input("What's your favorite snack?")
font = input("What's your go-to typing font?")
color = input("What's your favorite color?")
president1 = input("Who should be the president?")
president2 = input("Who's really the president?")
bestfriend = input("Who's your BEST friend?")
intuition = input("Let me test your intuition; what comes to mind when you hear the word: 'hell?'")
ride = input("What do you drive?")
season = input("What season is it?")
password = input("What's your password?")
nickname = input("What was your nickname in highschool?")
career = input("What's your career?")
colleague = input("Who's your colleague?")
cartoon = input("What's your favorite cartoon/cartoon character?")
boss = input("Who's your boss?")
number = input("Pick a number")
neighbor = input("Who's your neighbor?")
genre = input("What's your go-to genre?")
drink = input("What do you drink?")
chaser = input("What's your favorite liquid?")
crush = input("Who's your crush?")
cgender = input("What's your crush's gender?")

cgender = cgender.lower()
if cgender == "female" or cgender == "girl" or cgender == "woman" or cgender == "she" or cgender == "her":
	heshe = "she"
	himher = "her"
	hisher = "her"
	hishers = "hers"
elif cgender == "male" or cgender == "boy" or cgender == "man" or cgender == "guy" or cgender == "him" or cgender == "dude":
	heshe = "he"
	himher = "him"
	hisher = "his"
	hishers = "his"
else:
	print("Sorry, I'm a computer so I only do binary genders! I'm going to go with a default of female. Try typing 'female' or 'male' next time.\n")
	heshe = "she"
	himher = "her"
	hisher = "her"
	hishers = "hers"

group = input("What's your favorite political group to support?")
email = input("What's your email address?")

#conclusion
print("\nThanks for playing Pointless Interrogation(TM): SUNDAY(TM)! It's a Sunday morning at,",
formality.title(), user.title(), "'s residence.", "You get out of bed, pour yourself a tall glass of",
drink, ", and sit down at your computer.", "You look over your shoulder and see", neighbor.title(),
"peering into your window into your office.",
"You give him the finger and turn your computer screen away from his view, then secretively type '",
password, "' into the box on your login screen.",
"A bright red '1' pops up in the bottom right corner of your screen. It's an email from", boss, ".",
"A chill runs down your spine, because you remember jokingly slapping", colleague.title(),
"on the ass yesterday and you really don't want to get in trouble for workplace harassment at the",
career.title() + "'s office.", "You open the email and see it's just an office bet being discussed.",
colleague.title(), "is certain that", president1.title(), "is going to be president.", boss.title(),
"is willing to bet you and", colleague.title(), "a heafty sum of $" + number, "that", president2.title(),
"is going to win this election.", "You take a sip of", drink, "and think about the election.",
"You remember discussing the election with", crush.title(), "earlier this week.", heshe.title(),
"thinks", president2.title(), "is going to win. Oh how beautiful", crush.title(), "is.", hisher.title(),
"deep blue eyes make you want to go for a swim,", hisher,
"long, voluminous, golden blonde hair makes you want to climb Rapunzel's tower.",
"You shake yourself back to reality after daydreaming about", crush.title(), " for", number, "minutes.",
"You turn your attention back to the email. You know the polls have been saying that", president1.title(),
"has a", number + "%", "chance of winning the election this", season +
", but you decide to throw caution to the wide and go with your heart. You confidently type '" +
boss.lower() + "isonfire666@aol.com' and '" + colleague.lower() +
"thehairyunicorn@gmail.com' into the recipients box, and in the body you type '" +
"As true as", intuition, "is to hell as water is to ocean, Secretary", president2.title(),
"is going to be the next president of", residence.title() + ".",
"You look around you, see that", neighbor.title(), "is gone, log off your computer and go to rinse off your",
drink, "mug.", "Once you get to your kitchen, you look out your window at your prized possession, a 1993",
ride, "and see that some kids in to neighborhood egged it.", "Infuriated, you walk outside cursing: 'You rotten",
animal + "s!", "You look down at your", cartoon, "themed watch and realize it's time to leave for the",
group, "protest that you promised", crush.title(), "you would attend.",
"You ran inside to get the fliers you had prepared for the", group, "protest, then you hopped in your", ride,
"and put the pedal to the metal.", "When you arrived at the protest, you saw your crush laughing and smiling as",
heshe, "was talking to", enemy.title(), "Your arch enemy.", enemy.title(),
"Looked straight at you and smirked with pure evil as", crush.title(),
"pulled out a piece of paper and wrote something, probably", hisher, "phone number on it and handed it to",
enemy.title() + ".", "You tried to keep your cool as you walked through the densly populated", residence.title(),
"park. You handed out", number,
"fliers to random parkgoers before you even got to where all your friends were gathered to protest.",
"Once you got to your friends,", bestfriend.title(),
"ran up to you, jumped up in the air and met your torso in an attempted chestbump, and when you didn't also leap to the air,",
bestfriend.title(), "slapped you on the back and said: 'What the hell's wrong with you", nickname.title() + "?!",
"You shuddered in embarassment as you saw", crush.title(), "looking at you out of the corner of your eye,",
"and pulled", bestfriend.title(), "to the side to remind", bestfriend.title(), "that you're in love with",
crush.title(), "and that you think", heshe, "just gave", hisher, "number to", enemy.title() + ".", bestfriend.title(),
"said: 'Look,", user.title() + ",", enemy.title(), "got suspended from school for supergluing your ass to that toilet seat,",
"EVERYONE secretly hates", enemy.title() + "!'", "You know", bestfriend.title(), "is just trying to make you feel better.",
"Everyone thinks", enemy.title(), "is all that and a bag of", snack + ".", "You walk over to", crush.title(), "to show", himher,
"the fliers you designed. The fliers are", font, "font on a bright", color,
"background. They read: 'Support the", group, "today! Save lives, be happy! We need", number,
"new members to join by the end of the", season + "!", "Send us an email about why you care about", group,
"today at:", email + "'", crush.title(), "grabs the small stack of fliers that you hand to", himher,
"kisses them and says: 'They're gorgeous!'", "You ask", crush.title(), "what", himher, "and", enemy.title(),
"were chatting about, and", heshe, "says: 'Nothing! That asshole asked me for my phone number", number,
"times today, so finally I walked up and handed", enemy.title(),
"a note that read: 'Here's my number, cutie: 1-800-SHOVEITUPYOURASS, ", animal + "brains!!!",
"You coulnd't believe your ears. You threw caution to the wind, put one hand on", crush.title() + "'s",
"lower back and one on", hisher, "legs and scooped", himher, "into the air. You said: 'Wanna get outta here?",
"The two of you got", icecream, "icecream cones at the park, and only took", number, "licks before running up to",
enemy.title(), "and smashing them all in", enemy.title() + "'s pretty little face",
"The two of you threw your", font, "fliers into the air and announced to your friends that your not real", group,
"supporters, hopped into your", ride, "went to the local bar where you drank straight", drink, "and chased it with",
"smooth, delicious", chaser + ".\n\n\n", "This has been, Pointless Interrogation(TM!), episode 3, season 1: SUNDAY!",
"We hope you enjoyed! Play again soon!")
#^puts information from interrogation to use by giving the information back to the user in the form of a short story







