#introduction
user = input("Please enter your name") #learns user's name
print("\nWelcome,", user ,"to Pointless Interrogation(TM)!") #welcomes user to the program

#interrogation
color = input("What is your favorite color?") #learns user's favorite color
source = input("Where did you grow up?") #learns where the user grew up
pet = input("What is the name of your pet?") #learns the name of the user's pet
hair = input("What is the color of your hair?") #learns user's hair color
dream = input("What did you dream about last night?") #learns the dream the user had last night
phone = input("What is your phone number?") #learns user's phone number

#conclusion
print("\nThanks for playing Pointless Interrogation(TM)! I am now going to call you at",
      phone, "and tell you that your hair is", hair, "and that you should dye your hair",
      color, "because that's your favorite color. You should also take", pet,
      "on a road trip back to", source, "and write a novel about", dream)
#^puts information from interrogation to use by giving the information back to the user in the form of a short story 
