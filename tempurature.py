def CtoF(givenCelcius):
    '''
    Converts Celcius inputs to Farenheit outputs, and returns outputs
    '''
    print("I was given ", givenCelcius)
    resultFarenheit = (givenCelcius * 9/5) + 32
    return resultFarenheit

def FtoC(givenFarenheit):
    '''
    Converts Farenheit inputs to Celcius outpus, and returns outputs
    '''
    #print("I was given ", givenFarenheit)
    resultCelcius = (givenFarenheit - 32) * 5/9
    #print("I calculated ", resultCelcius)
    return resultCelcius

def userFtoC():
    while True:
        stringF = input("Please enter temperature in Farenheit: ")
        try:
            numF = float(stringF)
            numC = FtoC(numF)
            print("Converted to Celcius: ", numC)
            break
        except:
            print("Sorry, that doesn't look like a number")

def userCtoF():
    while True:
        stringC = input("Please enter temerature in Celcius: ")
        try:
            numC = float(stringC)
            numF = CtoF(numC)
            print("Converted to Farenheit: ", numF)
            break
        except:
            print("Sorry, that doesn't look like a number")

#print(CtoF.__doc__)
#print(FtoC.__doc__)
            
while True:
    temperatureUnitType = input("Choose input: type 'C' or 'F': ")
    if temperatureUnitType == "C" or temperatureUnitType == "c":
        print("Will ask for Celcius")
        userCtoF()
    elif temperatureUnitType == "F" or temperatureUnitType == "f":
        print("Will ask for Farenheit")
        userFtoC()
    else:
        print("Unknown temperature unit type", "'" + temperatureUnitType + "'")
