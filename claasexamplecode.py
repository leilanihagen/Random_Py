class Employee:
    'Common base class for all employees'
    empCount = 0

    def __init__(self, newname, newsalary):
        self.name = newname
        self.salary = newsalary
        Employee.empCount += 1

    def displayCount(self):
        print("Total Employee %d" % Employee.empCount)

    def prt(self):
        print("Name: ", self.name, ", Salary: ", self.salary)

class Pet:
    def __init__(self, newspecies, newweight):
        self.species = newspecies
        self.weight = newweight
        
    def prt(self):
        print("Species: ", self.species, ", Weight: ", self.weight)

emp1 = Employee('Bob Veila', 100*1000)
emp1.prt()
emp1.displayCount()

emp2 = Employee("Martha Stewart", 1)
emp2.prt()
emp2.displayCount()

emp1.displayCount()

pet1 = Pet("Drospophila Melanogaster", 0.001)
pet2 = Pet("CAT", 5000)

pet1.prt()
pet2.prt()
