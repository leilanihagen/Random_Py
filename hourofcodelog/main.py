import time

minsRunning = 0

userFile = input("Provide EMPTY writable file if this is the file's first entry, but reopen a previous session's file if needed: ")
f = open(userFile, 'a')

try:

	while True:
		time.sleep(5)
		minsRunning += 1
		print(str(minsRunning))

except:
	print(" Closing file...")
	f.write(str(minsRunning))
	f.close()